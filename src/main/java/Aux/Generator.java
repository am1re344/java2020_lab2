package Aux;

import Aux.FileActions.Create;
import Aux.FileActions.GetChildrenNumber;
import Aux.FileActions.GetName;
import Aux.FileActions.GetPath;
import FS.*;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class Generator {
    public Boolean DataForStressTestForDirectoriesAndBF(int iterations) {
        Directory rootDir = Directory.create("root", null);
        Controller controller = new Controller(10);

        for(int i = 1; i <= iterations; i++) {
            String dirName =  "Dir" + i;
            Directory dir = Directory.create(dirName, rootDir);
            controller.submitFSAction(new Create(rootDir, dir));
            if(i % 2 == 0) {
                BinaryFile binaryFile = new BinaryFile("bf" + i, rootDir, "content");
                controller.submitFSAction(new Create(rootDir, binaryFile));
            }

        }

        Integer DirCheck = 0, BFCheck = 0;
        for(int i = 0; i < rootDir.getChildrenCount(); i++) {
            if(rootDir.getChildren().get(i).getType() == "Directory")
                DirCheck++;
            if(rootDir.getChildren().get(i).getType() == "Binary file")
                BFCheck++;
        }
        if(DirCheck != iterations) return false;
        if(BFCheck != Math.floor(iterations/2)) return false;

        controller.shutdown();
        return true;
    }

    public Boolean DataForStressTestForGettingName() throws ExecutionException, InterruptedException {
        Directory rootDir = Directory.create("root", null);
        Controller controller = new Controller(10);

        Future<String> future = controller.submitFSAction(new GetName(rootDir));
        if(future.get() != "root") return false;
        controller.shutdown();
        return true;
    }

    public Boolean DataForStressTestForGettingPath() throws ExecutionException, InterruptedException {
        Directory rootDir = Directory.create("root", null);
        Controller controller = new Controller(10);
        Directory directory = Directory.create("somedir", rootDir);
        BufferFile bufferFile = new BufferFile("buffile", directory);

        Future<String> future = controller.submitFSAction(new GetPath(bufferFile));
        if(future.get() != "/root/somedir/buffile") return false;
        controller.shutdown();

        return true;
    }

    public Boolean DataForStressTestForGettingChildNum(int iterations) throws ExecutionException, InterruptedException {
        Directory rootDir = Directory.create("root", null);
        Controller controller = new Controller(10);

        for(int i = 1; i <= iterations; i++) {
            if(i % 2 == 0) {
                BinaryFile binaryFile = new BinaryFile("bf" + i, rootDir, "content");
                controller.submitFSAction(new Create(rootDir, binaryFile));
            }
            if(i % 3 == 0) {
                OwnNodeFile ownNodeFile = new OwnNodeFile("own" + i, rootDir, "cont");
                controller.submitFSAction(new Create(rootDir, ownNodeFile));
            }
            if(i % 4 == 0) {
                LogTextFile logTextFile = new LogTextFile("logtxt" + i, rootDir, "cont");
                controller.submitFSAction(new Create(rootDir, logTextFile));
            }

        }

        Future<Integer> future = controller.submitFSAction(new GetChildrenNumber(rootDir));
        if(future.get() != (Math.floor(iterations / 2) +
                Math.floor(iterations / 3) + Math.floor(iterations / 4))) return false;
        controller.shutdown();
        return true;
    }
}
