package Aux.FileActions;

import Aux.FSAction;
import FS.Directory;

public class GetChildrenNumber implements FSAction<Integer> {
    private Directory directory;

    public GetChildrenNumber(Directory directory) {
        this.directory = directory;
    }

    @Override
    public Integer perform() {
        try {
            return directory.getChildrenCount();
        } catch (Exception e) {
            throw e;
        }
    }
}
