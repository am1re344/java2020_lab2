package Aux.FileActions;

import Aux.FSAction;
import FS.Directory;
import FS.FileSystemNode;

public class Create implements FSAction<Boolean> {

    private Directory directory;
    private FileSystemNode fileSystemNode;


    public Create(Directory directory, FileSystemNode fileSystemNode) {
        this.fileSystemNode = fileSystemNode;
        this.directory = directory;
    }

    @Override
    public Boolean perform() {
        try {
            return directory.createFile(fileSystemNode);
        } catch (Exception e) {
            throw e;
        }
    }
}
