package Aux.FileActions;

import Aux.FSAction;
import FS.Directory;
import FS.FileSystemNode;

public class GetName implements FSAction<String> {
    private Directory directory;
    private FileSystemNode fileSystemNode;

    public GetName(FileSystemNode fileSystemNode) {
        this.fileSystemNode = fileSystemNode;
    }

    public GetName(Directory directory) {
        this.directory = directory;
    }

    @Override
    public String perform() {
        try {
            if(directory != null) return directory.getName();
            else if(fileSystemNode != null) return fileSystemNode.getName();
            else return null;
        } catch (Exception e) {
            throw e;
        }
    }
}
