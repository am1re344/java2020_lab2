package Aux.FileActions;

import Aux.FSAction;
import FS.Directory;
import FS.FileSystemNode;

public class GetParentDir implements FSAction<Directory> {
    private FileSystemNode fileSystemNode;

    public GetParentDir(FileSystemNode fileSystemNode) {
        this.fileSystemNode = fileSystemNode;
    }
    @Override
    public Directory perform() {
        try {
            return fileSystemNode.getDirectory();
        } catch (Exception e) {
            throw e;
        }
    }
}
