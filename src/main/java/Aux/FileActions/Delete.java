package Aux.FileActions;

import Aux.FSAction;
import FS.Directory;
import FS.FileSystemNode;

public class Delete implements FSAction<Boolean> {
    private Directory directory;
    private FileSystemNode fileSystemNode;


    public Delete(Directory directory, FileSystemNode fileSystemNode) {
        this.fileSystemNode = fileSystemNode;
        this.directory = directory;
    }

    @Override
    public Boolean perform() {
        try {
            directory.removeFile(fileSystemNode);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }
}
