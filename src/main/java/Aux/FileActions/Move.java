package Aux.FileActions;

import Aux.FSAction;
import FS.Directory;
import FS.FileSystemNode;

public class Move implements FSAction<Boolean> {
    private FileSystemNode fileSystemNode;
    private Directory directory;

    public Move(FileSystemNode fileSystemNode, Directory directory) {
        this.fileSystemNode = fileSystemNode;
        this.directory = directory;
    }

    @Override
    public Boolean perform() {
        try {
            if(fileSystemNode != null) {
                fileSystemNode.moveTo(directory);
                return true;
            }
            else return false;
        } catch (Exception e) {
            throw e;
        }
    }
}
