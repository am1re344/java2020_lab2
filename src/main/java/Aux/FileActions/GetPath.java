package Aux.FileActions;

import Aux.FSAction;
import FS.Directory;
import FS.FileSystemNode;

public class GetPath implements FSAction<String> {
    private FileSystemNode fileSystemNode;

    public GetPath(FileSystemNode fileSystemNode) {
        this.fileSystemNode = fileSystemNode;
    }

    @Override
    public String perform() {
        try {
            if(fileSystemNode != null) return fileSystemNode.getPath();
            else return null;
        } catch (Exception e) {
            throw e;
        }
    }
}
