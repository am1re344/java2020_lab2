package Aux.FileActions;

import Aux.FSAction;
import FS.FileSystemNode;

public class GetType implements FSAction<String> {
    private FileSystemNode fileSystemNode;

    public GetType(FileSystemNode fileSystemNode) {
        this.fileSystemNode = fileSystemNode;
    }

    @Override
    public String perform() {
        try {
            if(fileSystemNode != null) return fileSystemNode.getType();
            else return null;
        } catch (Exception e) {
            throw e;
        }
    }
}
