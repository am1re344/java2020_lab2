package Aux;

public interface FSAction<T> {
    T perform();
}
