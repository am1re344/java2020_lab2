package Aux;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Controller {
    ExecutorService workers;

    public Controller(int workerThreadsNum) {
        workers = Executors.newFixedThreadPool(workerThreadsNum);
    }

    public <T> Future<T> submitFSAction (FSAction action) {
        try {
            return (Future<T>) workers.submit(action::perform);
        } catch (Exception e) {
            throw e;
        }
    }

    public void shutdown() {
        workers.shutdown();
    }
}
