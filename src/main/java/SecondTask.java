import FS.*;

public class SecondTask {
    public static void main(String[] args) {
        Directory root = new Directory("root", null);

        Directory dir1 = Directory.create("dir1", root);
        Directory dir2 = Directory.create("dir2", root);
        Directory dir3 = Directory.create("dir3", root);
        Directory dir4 = Directory.create("dir4", root);

        Directory childDir1 = Directory.create("childDir1", dir1);
        Directory childDir2 = Directory.create("childDir2", dir1);

        Directory subChildDir1 = Directory.create("subChildDir1", childDir1);

        BinaryFile binaryFile = BinaryFile.create("first.bin", dir1, "s");
        BinaryFile binaryFile2 = BinaryFile.create("second.bin", dir2, "s");
        BinaryFile binaryFile3 = BinaryFile.create("third.bin", dir3, "s");

        LogTextFile logTextFile = new LogTextFile("log.1", dir1, "c");
        LogTextFile logTextFile2 = new LogTextFile("log.2", dir3, "c");

        BufferFile bufferFile = BufferFile.<String>create("buf1", dir4);
        BufferFile bufferFile2 = BufferFile.<String>create("buf2", dir4);

        OwnNodeFile ownNodeFile = OwnNodeFile.create("some name", dir3, "cont");
        OwnNodeFile ownNodeFile2 = OwnNodeFile.create("some name 2", dir4, "cont");
        OwnNodeFile ownNodeFile3 = OwnNodeFile.create("some name 3", dir4, "cont");
    }
}
