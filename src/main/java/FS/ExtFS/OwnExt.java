package FS.ExtFS;

import FS.Directory;
import FS.FileSystemNode;
import FS.OwnNodeFile;

import java.util.List;
import java.util.concurrent.RecursiveTask;

public class OwnExt extends RecursiveTask<Integer> {
    Directory directory;

    public OwnExt(Directory dir) {
        this.directory = dir;
    }

    @Override
    protected Integer compute() {
        Integer sum = 0;
        for (FileSystemNode child : directory.getChildren()) {
            if(child instanceof OwnNodeFile) {
                sum++;
            }
        }
        return sum;
    }
}
