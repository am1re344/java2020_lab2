package FS.ExtFS;

import FS.Directory;
import FS.FileSystemNode;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.RecursiveTask;


public class ThreeJsonExt extends RecursiveTask<JsonNode> {
    private Directory directory;
    private ObjectMapper mapper;

    public ThreeJsonExt(Directory dir) {
        this.directory = dir;
        this.mapper= new ObjectMapper();
    }


    @Override
    protected JsonNode compute() {
        ArrayNode root = mapper.createArrayNode();
        List<ThreeJsonExt> subTasks = new LinkedList<>();
        ObjectNode finalResult = mapper.createObjectNode();

        for (FileSystemNode child : directory.getChildren()) {
            JsonNode jsonNode = NodeToJson(child);
            if (child instanceof Directory) {
                ThreeJsonExt task = new ThreeJsonExt((Directory) child);
                task.fork();
                subTasks.add(task);
            } else root.add(jsonNode);
        }

        for (ThreeJsonExt task : subTasks) {
            JsonNode jsonNode = task.join();
            root.add(jsonNode);
        }

        finalResult.put(directory.getName(), root);
        return finalResult;
    }

    private JsonNode NodeToJson(FileSystemNode node){
        ObjectNode tmp = mapper.createObjectNode();
        tmp.put(node.getName(), node.getClass().getSimpleName());
        return tmp;
    }
}
