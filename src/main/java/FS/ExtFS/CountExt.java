package FS.ExtFS;

import FS.Directory;
import FS.FileSystemNode;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

public class CountExt extends RecursiveTask<Integer> {
    private Directory directory;

    public CountExt(Directory dir) {
        this.directory = dir;
    }

    @Override
    protected Integer compute() {
        Integer sum = directory.getChildrenCount();
        List<CountExt> countSubTasks = new LinkedList<>();

        for (FileSystemNode child : directory.getChildren()) {
            if(child instanceof Directory) {
                CountExt countExtTask = new CountExt((Directory) child);
                countExtTask.fork();
                countSubTasks.add(countExtTask);
            }
        }
        for (CountExt subtask : countSubTasks) sum += subtask.join();

        return sum;
    }

}
