package FS;

import java.util.LinkedList;
import java.util.Queue;

public class BufferFile<T> extends FileSystemNode {

    private int MAX_BUF_FILE_SIZE = 15;
    private Queue<T> elements;

    public BufferFile(String name, Directory parent) {
        super(name, "Buffer File", parent);
        elements = new LinkedList<>();
        parent.createFile(this);
    }

    public static <T> BufferFile<T> create(String name, Directory parent) {
        if(name.equals("") || (parent != null && parent.getChildrenCount() == parent.getDirMaxElems())) return null;
        return new BufferFile<>(name, parent);
    }

    public boolean push(T content) {
        if(elements.size() == MAX_BUF_FILE_SIZE) return false;
        else {
            elements.add(content);
            return true;
        }
    }

    public T consume() {
        return elements.poll();
    }

}
