package FS;

import java.util.ArrayList;
import java.util.List;

public class Directory extends FileSystemNode {

    private static int DIR_MAX_ELEMS = 15;
    private ArrayList<FileSystemNode> childFiles;

    public Directory(String name, Directory parent) {
        super(name, "Directory", parent);
        childFiles = new ArrayList<>();
        if(parent != null)
            parent.createFile(this);; //add
    }

    public static Directory create(String name, Directory parent) {
        if(name.equals("") || (parent != null && parent.getChildrenCount() == DIR_MAX_ELEMS)) return null;
        return new Directory(name, parent);
    }

    public int getChildrenCount() {
        return childFiles.size();
    }

    public boolean createFile(FileSystemNode fileSystemNode) {
        if(fileSystemNode != null) {
            if(!childFiles.contains(fileSystemNode) && getChildrenCount() < DIR_MAX_ELEMS) {
                childFiles.add(fileSystemNode);
                return true;
            }
        }
        return false;
    }

    public void removeFile(FileSystemNode fileSystemNode) {
        childFiles.remove(fileSystemNode);
    }

    @Override
    public void moveTo(Directory directory) {
        super.moveTo(directory);
    }

    public ArrayList<? super FileSystemNode> getChildFiles() {
        return (ArrayList<? super FileSystemNode>)childFiles.clone();
    }
    public ArrayList<FileSystemNode> getChildren() {
        return childFiles;
    }

    public static int getDirMaxElems() {
        return DIR_MAX_ELEMS;
    }
}
