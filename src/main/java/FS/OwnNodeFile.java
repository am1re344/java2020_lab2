package FS;

public class OwnNodeFile extends FileSystemNode {

    private String content;

    public OwnNodeFile(String name, Directory parent, String content) {
        super(name, "Own Node File", parent);
        this.content = content;
    }

    public static OwnNodeFile create(String name, Directory parent, String content) {
        if(name.equals("") || (parent != null && parent.getChildrenCount() < parent.getDirMaxElems())) return null;
        return new OwnNodeFile(name, parent, content);
    }

    public String read() {
        return content;
    }

    public void rewrite(String content) {
        this.content = content;
    }

    public void append(String additionalContent) {
        this.content += additionalContent;
    }

}
