package FS;

public class BinaryFile extends FileSystemNode {

    private String content;

    public BinaryFile(String name, Directory parent, String content) {
        super(name, "Binary file", parent);
        this.content = content;
        parent.createFile(this);
    }

    public BinaryFile(String name, String content) {
        super(name);
        this.content = content;
    }

    public static BinaryFile create(String name, Directory parent, String content) {
        if(name.equals("") || (parent != null && parent.getChildrenCount() < parent.getDirMaxElems())) return null;
        return new BinaryFile(name, parent, content);
    }

    public String read() {
        return content;
    }
}
