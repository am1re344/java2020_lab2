package FS;

public class LogTextFile extends FileSystemNode {

    private String content;

    public LogTextFile(String name, Directory parent, String content) {
        super(name, "Log Text File", parent);
        this.content = content;
        parent.createFile(this);
    }

    public static LogTextFile create(String name, Directory parent, String content) {
        if(name.equals("") || (parent != null && parent.getChildrenCount() == parent.getDirMaxElems()) || parent == null) return null;
        return new LogTextFile(name, parent, content);
    }

    public void Append(String additionalContent) {
        this.content += additionalContent;
    }

    public String read() {
        return content;
    }
}
