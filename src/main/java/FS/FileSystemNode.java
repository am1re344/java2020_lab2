package FS;

public class FileSystemNode {

    private String name;
    private String type;
    private Directory parent;

    public FileSystemNode(String name, String type, Directory parent) {
        this.name = name;
        this.type = type;
        this.parent = parent;
    }

    public FileSystemNode(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Directory getDirectory() {
        return parent;
    }

    public void setDirectory(Directory directory) {
        this.parent = directory;
    }

    public void remove() {
        if(parent == null) return;
        parent.removeFile(this);
        parent = null;
    }

    public void moveTo(Directory directory) {
        parent.removeFile(this);
        parent = directory;
        directory.createFile(this);
    }

    public String getPath() {
        if(parent == null) return "/" + getName();
        return ((FileSystemNode)parent).getPath() + "/" + getName();
    }
}
