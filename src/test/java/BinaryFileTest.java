import FS.BinaryFile;
import FS.Directory;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BinaryFileTest {

    @Test
    public void Read_ReadEmptyContent_ShouldBeEqualsWithEmptyString() {
        BinaryFile bf = new BinaryFile("f.bin", "");
        String expected = "";
        assertEquals(expected, bf.read());
    }

    @Test
    public void Read_ReadWithContent_ShouldReturnWithContent() {
        BinaryFile bf = new BinaryFile("f.bin", "aAa");
        String expected = "aAa";
        assertEquals(expected, bf.read());
    }

    @Test
    public void GetParent_GetParentOfFile_ShouldReturnNull() {
        BinaryFile bf = new BinaryFile("f.bin", "aAa");
        assertNull(bf.getDirectory());
    }

    @Test
    public void GetParent_GetParentOfFile_ShouldReturnParentDirectory() {
        Directory d = new Directory("dir", null);
        BinaryFile bf = new BinaryFile("f.bin", d,"aAa");
        assertEquals(d, bf.getDirectory());
    }
}
