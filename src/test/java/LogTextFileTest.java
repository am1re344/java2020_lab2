import FS.*;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class LogTextFileTest {
    @Test
    public void Create_CreateEmpty_ShouldBeEmptyContent() {
        Directory root = Directory.create("root", null);
        LogTextFile logTextFile = LogTextFile.create("log", root, "");
        String exp = "";
        assertEquals(exp, logTextFile.read());
    }

    @Test
    public void Create_CreateNonEmpty_ContentShouldBeEqual() {
        Directory root = Directory.create("root", null);
        LogTextFile logTextFile = LogTextFile.create("log", root, "content");
        String exp = "content";
        assertEquals(exp, logTextFile.read());
    }

    @Test
    public void Append_AppendToContent_ContentShouldBeEqual() {
        Directory root = Directory.create("root", null);
        LogTextFile logTextFile = LogTextFile.create("log", root, "content");
        logTextFile.Append(" add");
        String exp = "content add";
        assertEquals(exp, logTextFile.read());
    }

    @Test
    public void Create_CreateWithEmptyNameTest_ShouldReturnNull() {
        Directory root = Directory.create("root", null);
        assertNull(LogTextFile.create("", root, "content"));
    }

    @Test
    public void Create_CreateWithEmptyParent_ShouldReturnNull() {
        Directory root = Directory.create("root", null);
        assertNull(LogTextFile.create("qwerty", null, "content"));
    }
}
