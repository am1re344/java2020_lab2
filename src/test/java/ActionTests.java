
import Aux.FileActions.*;
import FS.Directory;
import FS.FileSystemNode;
import FS.OwnNodeFile;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class ActionTests {
    private Directory rootDir;

    @BeforeEach
    public void createRootDir() {
        rootDir = Directory.create("root", null);
    }

    @Test
    public void Create_CreateFile_ShouldCreateFile() {
        FileSystemNode file = new OwnNodeFile("name", rootDir, "content");
        Create createFile = new Create(rootDir, file);
        assertTrue(createFile.perform());
    }

    @Test
    public void Delete_DeleteFile_ShouldCreateFile() {
        FileSystemNode file = new FileSystemNode("name","type" ,rootDir );
        Create createFile = new Create(rootDir, file);
        assertTrue(createFile.perform());
        Delete deleteFile = new Delete(rootDir, file);
        assertTrue(deleteFile.perform());
    }

    @Test
    public void GetChildNum_GetChildNum_ShouldReturnChildNum() {
        rootDir.createFile(new FileSystemNode("name","type" ,rootDir));
        assertEquals(1, rootDir.getChildrenCount());
    }

    @Test
    public void GetName_GetFileName_ShouldReturnFileName() {
        FileSystemNode file = new FileSystemNode("name","type" , rootDir);
        GetName getName = new GetName(file);
        assertEquals("name", getName.perform());
    }

    @Test
    public void GetName_GetNullDirectoryName_ShouldReturnNull() {
        GetName getName = new GetName(null);
        assertNull(getName.perform());
    }

    @Test
    public void GetName_GetNullFileName_ShouldReturnNull() {
        FileSystemNode file = null;
        GetName getName = new GetName(file);
        assertNull(getName.perform());
    }


    @Test
    public void GetChildNum_GetChildNumOfDir_ShouldReturnZero() {
        Integer exp = 0;
        GetChildrenNumber getChildrenNumber = new GetChildrenNumber(rootDir);
        assertEquals(exp, getChildrenNumber.perform());
    }

    @Test
    public void GetParentDir_GetRootDir_ShouldReturnRoot() {
        FileSystemNode file = new FileSystemNode("name","type" , rootDir);
        GetParentDir getParentDir = new GetParentDir(file);
        assertEquals(rootDir, getParentDir.perform());
    }

    @Test
    public void GetType_GetFileType_ShouldReturnRightType() {
        FileSystemNode file = new FileSystemNode("name","type" , rootDir);
        GetType getType = new GetType(file);
        assertEquals("type", getType.perform());
    }

    @Test
    public void Move_MoveToAnotherFolder_ShouldMove() {
        FileSystemNode file = new FileSystemNode("name","type" , rootDir);
        Move move = new Move(file, rootDir);
        assertTrue( move.perform());
    }

    @Test
    public void Move_MoveToAnotherFolderNull_ShouldNotMove() {
        Move move = new Move(null, rootDir);
        assertFalse( move.perform());
    }

    @Test
    public void getPath_GetNullPath_ShouldReturnNull() {
        FileSystemNode file = null;
        GetPath getPath = new GetPath(file);
        assertNull(getPath.perform());
    }

    @Test
    public void getPath_GetFullPath_ShouldReturnRightPath() {
        FileSystemNode file = new FileSystemNode("name","type" , rootDir);
        GetPath getPath = new GetPath(file);
        assertEquals("/root/name",getPath.perform());
    }
}
