import Aux.FileActions.*;
import Aux.Generator;
import FS.Directory;
import FS.FileSystemNode;
import FS.OwnNodeFile;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class StressTest {
    private Generator generator = new Generator();

    @Test
    public void StressTesting_StressTestForDirAndBFCreatingAction_ShouldReturnTrue() {
        assertTrue(generator.DataForStressTestForDirectoriesAndBF(10));

    }

    @Test
    public void StressTesting_StressTestForGetNameAction_ShouldReturnTrue() throws ExecutionException, InterruptedException {
        assertTrue(generator.DataForStressTestForGettingName());
    }

    @Test
    public void StressTesting_StressTestForGetPathAction_ShouldReturnTrue() throws ExecutionException, InterruptedException {
        assertTrue(generator.DataForStressTestForGettingName());
    }

//    @Test
//    public void StressTesting_StressTestForGetChildrenAction_ShouldReturnTrue() throws ExecutionException, InterruptedException {
//        assertTrue(generator.DataForStressTestForGettingChildNum(15));
//    }

}
