import FS.BinaryFile;
import FS.BufferFile;
import FS.Directory;
import FS.FileSystemNode;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

public class DirectoryTest {
    @Test
    public void Create_CreateDirectoryWithName_NamesShouldBeEquals() {
        Directory directory = Directory.create("testDir", null);
        String exp = "testDir";
        assertEquals(exp, directory.getName());
    }

    @Test
    public void Create_CreateDirectoryWithParent_ParentsShouldBeEquals() {
        Directory root = Directory.create("root", null);
        Directory directory = Directory.create("testDir", root);
        String exp = "root";
        assertEquals(exp, directory.getDirectory().getName());
    }

    @Test
    public void Insert_InsertFileInDirectory_NamesOfFilesShouldBeEqual() {
        Directory root = Directory.create("root", null);
        Directory someDir = Directory.create("dir", root);
        root.createFile(someDir);
        assertTrue(root.getChildFiles().contains(someDir));

    }


}
