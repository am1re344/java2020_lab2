import FS.BufferFile;
import FS.Directory;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BufferFileTest {

    @Test
    public void Read_ReadEmptyContent_ShouldBeEqualsWithNull() {
        Directory d = new Directory("dir", null);
        BufferFile bf = new BufferFile("buf",d);
        assertNull(bf.consume());
    }

    @Test
    public void Read_ContainsOneElement_ContentShouldNotBeEmpty(){
        Directory d = new Directory("dir", null);
        BufferFile bf = new BufferFile("buf",d);

        bf.push("str");

        assertEquals("str", bf.consume());
        assertNull(bf.consume());
    }

    @Test
    public void GetParent_GetParentOfFile_ShouldReturnParentDirectory() {
        Directory d = new Directory("dir", null);
        BufferFile bf = new BufferFile("buf",d);
        assertEquals(d, bf.getDirectory());
    }

    @Test
    public void Read_ReadACoupleOfElements() {
        Directory d = new Directory("dir", null);
        BufferFile bf = new BufferFile("buf", d);

        for(int i = 0; i < 5; i++)
            bf.push(i + " file");

        for(int i = 0; i < 5; i++)
            assertEquals(i + " file", bf.consume());
    }
}
